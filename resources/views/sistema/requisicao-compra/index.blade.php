@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-sm-6">
            <a href="{{ route('home') }}" class="btn btn-dark">
                <i class="fa fa-reply"></i> Voltar
            </a>
        </div>
        <div class="col-sm-6 pull-left">
            <a href="{{ route('requisicao-compra.create') }}" class="btn btn-primary pull-right">
                <i class="fa fa-plus"></i> Cadastrar
            </a>
        </div>
    </div>

    <div class="titulo-cadastros">
        <h2>Lista de requisições de compras</h2>
    </div>

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Descrição</th>
            <th scope="col">Data</th>
            <th scope="col">Status</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

@endsection
