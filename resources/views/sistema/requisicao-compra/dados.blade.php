@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-sm-10">
            <h2><i class="fa fa-archive"></i> Requisição de compra</h2>
        </div>
        <div class="col-sm-2">
            <a href="{{ route('requisicao-compra.index') }}" class="btn btn-secondary pull-right">
                <i class="fa fa-reply"></i> Voltar
            </a>
        </div>
    </div>

    <form role="form" method="post" action="POST" enctype="multipart/form-data">

        {!! Route::is('*.show') ? method_field('put') : '' !!}
        {!! csrf_field() !!}
        {!! csrf_field() !!}

<?php if (!empty($dados)) : ?>
        <div class="margin-top-botton-25">
        <a href="{{ route('indicador.destroy', ['id' => $dados->id]) }}"
           class="btn btn-outline-danger btn-sm btn-excluir-indicador">
            <i class="fa fa-remove"></i> Excluir</a>
        </div>
<?php endif; ?>
        <div class="card margin-top-35">
            <div class="card-header">

                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Descrição:</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="descricao" placeholder="Escreva a descrição"
                        value="{{ !empty($dados) ? $dados->descricao : old('descricao') }}">
                        @include('layouts.partials.helper-error', ['field' => 'descricao'])
                    </div>

                    <label for="inputPassword" class="col-sm-2 col-form-label">Staus:</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="is_ativo">
                            @if (isset($dados->is_ativo))
                                <option value="0" {{$dados->is_ativo == 0 ? 'selected' : '' }}>
                                    Inativo
                                </option>
                                <option value="1" {{$dados->is_ativo == 1 ? 'selected' : '' }}>
                                    Ativo
                                </option>
                            @else
                                <option value="1" selected>Ativo</option>
                                <option value="0">Inativo</option>
                            @endif
                        </select>
                        @include('layouts.partials.helper-error', ['field' => 'is_ativo'])
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Prioridade:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="prioridade" id="txtPrioridade"
                               placeholder="Escreva a prioridade" maxlength="2"
                               value="{{ !empty($dados) ? $dados->prioridade : old('prioridade') }}">
                               @include('layouts.partials.helper-error', ['field' => 'prioridade'])
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Indice de porcentagem:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="indice_porcentagem" id="txtIndice"
                               placeholder="Escreva a porcentagem" maxlength="3"
                               value="{{ !empty($dados) ? $dados->indice_porcentagem : old('indice_porcentagem') }}">
                        @include('layouts.partials.helper-error', ['field' => 'indice_porcentagem'])
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="panel-footer">
                    <a class="btn btn-warning" href="{{ route('requisicao-compra.index') }}"><i class="fa fa-times"></i> Cancelar</a>
                    <button type="submit" class="btn btn-success pull-right">
                        <i class="fa fa-fw fa-floppy-o"></i> Salvar
                    </button>
                </div>

            </div>
        </div>

    </form>
</div>
@endsection
