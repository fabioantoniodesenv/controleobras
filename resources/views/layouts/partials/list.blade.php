@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ $title }}
                <a class="btn btn-primary btn-xs pull-right" href="{{ $route_create }}"><i class="fa fa-plus"></i> Novo</a>
            </div>

            <div class="panel-body">
                <table class="table table-striped table-hover" id="dataTables-example">
                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div id="{{ $id_grid }}" class="autoheight" data-route_list="{{ $route_list }}"></div>

                        </div>
                    </div>
                </table>
            </div>

        </div>
    </div>

@endsection