@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <div class="card text-white bg-secondary mb-3" style="max-width: 18rem;">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            Últimas requisições
                        </div>
                        <div class="col-sm-6">
                            <a class="pull-right btn btn-light btn-sm" href="{{ route("requisicao-compra.index") }}">
                                Abrir todas
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <p>Aqui vai os últimos desta categoria</p>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            Últimos orçamentos
                        </div>
                        <div class="col-sm-6">
                            <a class="pull-right btn btn-light btn-sm" href="{{ route("orcamento.index") }}">
                                Abrir todos
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <p>Aqui vai os últimos desta categoria</p>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            Últimas compras
                        </div>
                        <div class="col-sm-6">
                            <a class="pull-right btn btn-light btn-sm" href="{{ route("compras-produtos.index") }}">
                                Abrir todas
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <p>Aqui vai os últimos desta categoria</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
