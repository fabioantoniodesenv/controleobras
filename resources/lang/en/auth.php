<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Não foi encontrado nenhum usuário com este registro.',
    'throttle' => 'Você tentou muitas vezes. Tente novamente em :seconds segundos.',

];
