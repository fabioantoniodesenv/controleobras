<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function() {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['prefix' => 'requisicao-compra'], function () {

        Route::get('')->uses('Sistema\RequisicaoCompraController@index')->name('requisicao-compra.index');
        Route::get('create/')->uses('Sistema\RequisicaoCompraController@create')->name('requisicao-compra.create');
        Route::get('show/{id}')->uses('Sistema\RequisicaoCompraController@show')->name('requisicao-compra.show');
        Route::post('')->uses('Sistema\RequisicaoCompraController@store')->name('requisicao-compra.store');
        Route::put('{id}')->uses('Sistema\RequisicaoCompraController@update')->name('requisicao-compra.update');
        Route::get('delete/{id}')->uses('Sistema\RequisicaoCompraController@destroy')->name('requisicao-compra.destroy');
    });

    Route::group(['prefix' => 'compras'], function () {

        Route::get('')->uses('Sistema\ComprasController@index')->name('compras-produtos.index');
        Route::get('create/')->uses('Sistema\ComprasController@create')->name('compras-produtos.create');
        Route::get('show/{id}')->uses('Sistema\ComprasController@show')->name('compras-produtos.show');
        Route::post('')->uses('Sistema\ComprasController@store')->name('compras-produtos.store');
        Route::put('{id}')->uses('Sistema\ComprasController@update')->name('compras-produtos.update');
        Route::get('delete/{id}')->uses('Sistema\ComprasController@destroy')->name('compras-produtos.destroy');
    });

    Route::group(['prefix' => 'orcamento'], function () {

        Route::get('')->uses('Sistema\OrcamentosController@index')->name('orcamento.index');
        Route::get('create/')->uses('Sistema\OrcamentosController@create')->name('orcamento.create');
        Route::get('show/{id}')->uses('Sistema\OrcamentosController@show')->name('orcamento.show');
        Route::post('')->uses('Sistema\OrcamentosController@store')->name('orcamento.store');
        Route::put('{id}')->uses('Sistema\OrcamentosController@update')->name('orcamento.update');
        Route::get('delete/{id}')->uses('Sistema\OrcamentosController@destroy')->name('orcamento.destroy');
    });

//    Route::group(['prefix' => 'indice'], function () {
//        Route::get('')->uses('IndiceController@index')->name('indice.index');
//    });



});

